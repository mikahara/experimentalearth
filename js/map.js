var ge;
var experimental = experimental || {};
experimental.earth = experimental.earth || {};

experimental.earth.init = function(){

      	google.load("earth", "1");
	
	function init() {
		google.earth.createInstance('map3d', initCB, failureCB);
	}

	function initCB(instance) {
         	ge = instance;
         	ge.getWindow().setVisibility(true);
		ge.getNavigationControl().setVisibility(ge.VISIBILITY_AUTO);
   		ge.getOptions().setTerrainExaggeration(2.2);
		ge.getSun().setVisibility(true);
      	}

      	function failureCB(errorCode) {
		alert("ERROR: " + errorCode);      	
	}

	return google.setOnLoadCallback(init);
} 
